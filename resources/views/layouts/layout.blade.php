<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Luxury Services recruitment & services</title>
    @include('partials/head')
</head>

<body id="top">

    @include('partials/header')

    <div class="base">
        <!-- Page -->
        <div class="page">
            @yield('content')
        </div>
    </div>

    @include('partials/footer')

    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.modernizr.1c9d48a1e28b608c156f4de214d48a4f_1513864496.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery.5b69aaaa5f04c52e66580a2dc32d1d69_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.animated-headline.88cb936096e0f8ab70515bc801d8342e_1513864493.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.bootstrap-tabcollapse.34017c51086f0e87aa46196565972d00_1513864493.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.coundown-timer.c8cf6063f2de9fb102572c2c5cbb35d9_1518615831.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.equalheight.317d8490808d596c8a0f1758d495856c_1513864493.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.menuzord.1b3c610e36a938dda7612403eff6e3cf_1513864496.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.smooth-menu.561efb1e63c25f6459bb7d6b910fd0e0_1513864497.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.imagesloaded.cd624dbd9f9cedc0517e2a007e5a9e99_1513864494.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.owl-carousel.9a2fb1bcafcaeab824262566db2bddef_1513864521.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.wow.8dcaf0f70a4ccabe591dd6faf3c07bdc_1513864497.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.scripts.b4ebfafba9aad4e481cd55b4053b8783_1523435133.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.bootstrap.457bf879eca11f66e2de154003ed0f50_1513863978.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.materialize.bfaa8e3bbc89c450471bc8cc1b9a0959_1513864503.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-easing.502fedff28c30ca28b69b469d4f49248_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-countTo.2216a6bac856d869218448a54e6bbc8c_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-sticky.980853b199a69acce57cf93d5a9a3767_1513864496.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-stellar.55e412525b98bd15386ee154989c3e48_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-inview.0fee8e871d8e8332b973955f71c14a0b_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-shuffle.59f156c2072439edc7423e30643fed01_1520436199.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-magnific-popup.5830d3754f9847bc3affd25657b7464e_1513864498.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-flexslider.6fd2216059a696cd152f2e674c4e2866_1513863989.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-tools.13f52e879d891fbc0072882de2388d55_1513864531.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-scripts.1d7d66239bf5d82bb032d5316a4e65f8_1513864531.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-video.411acb0dd4c25380bc5cc802d38f611f_1513864557.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-slideanims.b7012bec777aae6bf942e6b3de7eb74d_1513864557.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-actions.e3fe630f2ef135883640f4b9287af8cd_1513864555.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-layeranimation.9be5c338de7d5dd501d7abbcda0fbf3b_1513864556.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-kenburn.ff163220ac1c94be65aa01cd40ae3062_1513864555.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-navigation.2ed69f3ed2a71a822ae8cb9db68f948b_1513864556.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-migration.f05042883f3b2c4d6fdc843e9d033d3a_1513864556.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.revolution-ext-parallax.402830701f2cb1475353aa7335920e6b_1513864556.js"></script>

    <!-- RS5.0 Init  -->
    <script type="text/javascript">
    $(function () {
    $('select').material_select();
})
        let slider = document.querySelector(".rev_slider_wrapper").setAttribute("style",
            "background:url('img"+(
                isMobile ?
                    "/bg1.jpg":
                    "/bg3.jpg"
            ) + "');" +
            "background-size:cover;"+
            "background-position: center center;"+
            "height:"+window.innerHeight+"px"
        );

        onAppReady(function() {
            $('.preload').remove();
            $(".materialize-slider").revolution({
                sliderType:"standard",
                sliderLayout:"fullscreen",
                delay:20000,
                navigation: {
                    keyboardNavigation: "on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    }
                },
                responsiveLevels:[1240,1024,778,480],
                gridwidth:[1240,1024,778,480],
                gridheight:[700,600,500,500],
                disableProgressBar:"on",
                parallax: {
                    type:"mouse",
                    origo:"slidercenter",
                    speed:2000,
                    levels:[2,3,4,5,6,7,12,16,10,50],
                }

            });
        });
        
    </script>

</body>
</html>
