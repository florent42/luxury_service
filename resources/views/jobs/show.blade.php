@extends('layouts.layout')
@section('content')
    <!-- Page Header-->
    {{-- <section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="white-text"> {{$offerId['title']}}</h2>
                    <span class="white-text">Ref. {{$offerId['id']}} </span>
                    <ol class="breadcrumb">
                        <li>
                            <div class="portfolio-nav">
                                <a href="#!" class="waves-effect waves-dark"><i class="fa fa-angle-left" aria-hidden="true"></i> Prev</a>
                                <a href="#!" class="waves-effect waves-dark"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                                <a href="#!" class="waves-effect waves-dark">Next <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section> --}}

    <!-- Page Content-->
    <section class="single-project-section section-padding light-gray-bg">
        <div class="container">
            <div class="project-overview">
                <div class="row mb-80">
                    <div class="col-xs-12 col-md-8">
                        <p class="ref grey-text no-margin">Ref. {{$offerId['id']}}</p>
                        <h2>{{$offerId['title']}}</h2>
                        <p> {{$offerId['description']}}</p>
                    </div>

                    <div class="col-xs-12 col-md-4 quick-overview">
                        <ul class="portfolio-meta">
                            <li><span> Pulished at </span> {{$offerId['created_at']}}</li>
                            <li><span> Contract Type </span> {{$offerId['offer_time_type']}}</li>
                            <li><span> Salary </span> {{$offerId['salary']}} &euro;</li>
                            <li><span> Location </span> {{$offerId['location']}}</li>
                            <li><span> Category </span> {{$offerId['category']}}</li>
                            
                        </ul>

                        <form method="post" action="{{route('apply')}}">
                            <button class="btn btn-block gradient primary mt-30 waves-effect waves-light" href="#!">Apply for this job </button>
                        
                        </form>
                        
                        {{-- <div class="btn btn-block btn-success mt-30 waves-effect waves-light disabled">You have applied for this job</div> --}}
                    </div>
                </div>
            </div>

            <nav class="single-post-navigation no-margin" role="navigation">
                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="previous-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="#!">
                                <i class="fa fa-long-arrow-left"></i>Previous
                            </a>
                        </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-4"></div>


                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="next-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="#!">
                                Next <i class="fa fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </nav>
        </div>
    </section>

    

@endsection('content')