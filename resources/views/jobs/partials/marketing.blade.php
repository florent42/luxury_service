
@foreach ($offerMarketing as $offer)
    <div class="portfolio-item" data-groups='["marketing"]'>
        <div class="portfolio-wrapper">
            <div class="card job-card">
                <div class="card-content">
                    <span class="title">
                        <span class="card-title"> {{$offer['title']}} </span>
                        <span class="ref grey-text"> Ref. {{$offer['id']}}</span>
                    </span>
                    <div class="metas mb-20">
                        <div class="meta">
                            <i class="material-icons">&#xE53E;</i> {{$offer['salary']}}k&euro;
                        </div>
                        <div class="meta">
                            <i class="material-icons">&#xE916;</i> {{$offer['created_at']->diffForHumans()}}
                        </div>
                        <div class="meta">
                            <i class="material-icons">&#xE55F;</i> {{$offer['location']}}
                        </div>
                        <div class="meta">
                            <i class="material-icons">business_center</i> {{$offer['category']}}
                        </div>
                    </div>
                    <p class="truncate-text"> {{$offer['description']}}</p>
                </div>
                <div class="card-action">
                    <a class="btn btn-md primary border waves-effect waves-dark" href="{{ url('/jobs/show', $offer->id) }}">Details</a>

                    <form method="post" action="{{route('apply')}}">
                    @csrf
                        <button class="btn btn-md primary waves-effect waves-light" type="submit" value="{{$offer->id}}" name="offer_id">Apply</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endforeach