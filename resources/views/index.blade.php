@extends('layouts.layout')

@section('content')
    <!-- RS5.0 Core JS Files -->
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems! The following part can be removed on Server for On Demand Loading) -->
    <!-- start revolution slider 5.0 -->
    <section class="rev_slider_wrapper">
        @include('partials/slider')
    </section><!-- end of slider wrapper -->

    <!-- Page Content-->
    <!--<section class="full-width promo-box brand-bg ptb-50">-->
        <!--<div class="container">-->
            <!---->
        <!--</div>-->
    <!--</section>-->
                    @if (\Session::has('erroradmin'))
                    <div class="alert  red alert-succes">
                        <ul>
                            <li>{{ \Session::get('erroradmin') }}</li>
                        </ul>
                    </div>
                @endif
    <section class="ptb-50 brand-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-40">
                    <div class="promo-info">
                        <span class="white-text">Whether you're an employer or a candidate looking for jobs</span>
                        <h3 class="white-text text-bold text-uppercase no-margin">WE HAVE THE SOLUTION FOR YOU</h3>
                    </div>
                    <div class="promo-btn">
                        <a href="{{ url('/contact') }}" class="btn border secondary waves-effect waves-light">Contact Us</a>
                    </div>
                </div>
            </div>
            <hr class="mt-10 mb-50">
            <div class="text-center">
                <h2 class="section-title primary-text">Who We are</h2>
                <p class="section-sub white-text">Luxury Services is a leading professional recruitment consultancy specialising in the recruitment of permanent, contract and temporary positions on behalf of the world’s top employers.</p>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div id="project-slider" class="carousel slide boot-slider" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#project-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#project-slider" data-slide-to="1"></li>
                        </ol>

                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img class="img-responsive" src="img/slide1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="img/slide2.jpg" alt="">
                            </div>
                        </div>

                        <a class="left carousel-control" href="#project-slider" role="button" data-slide="prev">
                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#project-slider" role="button" data-slide="next">
                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h3 class="primary-text">Our philosophy</h3>
                    <p class="white-text">Our role is to work with employers and job seekers to facilitate a successful match. This can range from advising a global company on a candidate sourcing strategy to helping a job seeker find their dream job.</p>
                </div>
                <div class="text-center">
                    <a href="{{ url('/about-us') }}" class="btn gradient secondary waves-effect waves-light mt-40">More about us</a>
                </div>
            </div>
        </div>
    </section>
    <section class="cta-candidate bg-fixed bg-cover overlay dark-5 padding-top-70 padding-bottom-50">
        <div class="container">
            <div class="row">
                <div class="valign-wrapper text-center">
                    <div class="hero-intro valign-cell">
                        <h2 class="tt-headline clip is-full-width no-margin">
                            <span>You are </span>
                            <span class="tt-words-wrapper">
                                <b class="is-visible">Commercial</b>
                                <b>Creative</b>
                                <b>Marketing & PR</b>
                                <b>Technology</b>
                                <b>Fashion & luxury</b>
                                <b>Retail sales</b>
                            </span>
                        </h2>
                        <h3 class="c-secondary mb-30 no-margin">Sign-up and apply for jobs now</h3>
                        <p class="white-text section-sub">Each one of your skills is seen as precious resource to us, each one of your personality traits are considered as an added on value. Joining us is allowing our recruitment expertise and personal guidance to bring the best out in you by choosing the most suited position.</p>
                        <a href="{{ url('/register') }}" class="btn border secondary waves-effect waves-light mt-40">Join us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="job-offers" class="section-padding gray-bg">
        <div class="container">
            <div class="text-center mb-40">
                <h2 class="section-title">Job offers</h2>
                <p class="section-sub">Just like our candidates, we offer quality, serious & reliable positions. Come discover them and we will put you in contact with the perfect team for you to form the perfect crew.</p>
            </div>
            <div class="portfolio-container">
                <ul class="portfolio-filter brand-filter text-center">
                    <li class="active waves-effect waves-light" data-group="all">All</li>
                    <li class=" waves-effect waves-light" data-group="commercial">Commercial</li>
                    <li class=" waves-effect waves-light" data-group="retail">Retail sales</li>
                    <li class=" waves-effect waves-light" data-group="creative">Creative</li>
                    <li class=" waves-effect waves-light" data-group="technology">Technology</li>
                    <li class=" waves-effect waves-light" data-group="marketing">Marketing & PR</li>
                    <li class=" waves-effect waves-light" data-group="fashion">Fashion & luxury</li>
                    <li class=" waves-effect waves-light" data-group="management">Management & HR</li>
                </ul>
                <div class="portfolio portfolio-with-title col-2 gutter mt-30">
                    <div class="portfolio-item" data-groups='["all", "commercial"]'>
                        <div class="portfolio-wrapper">
                            <div class="card job-card">
                                <div class="card-content">
                                    <span class="title">
                                        <span class="card-title">2nd assistant position</span>
                                        <span class="ref grey-text">Ref. 1062</span>
                                    </span>
                                    <div class="metas mb-20">
                                        <div class="meta">
                                            <i class="material-icons">&#xE53E;</i>45k&euro;
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE916;</i>2018-10-01
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE55F;</i>Paris
                                        </div>
                                    </div>
                                    <p class="truncate-text">Nulla interdum, erat et mollis eleifend, urna turpis pellentesque dolor, sed rhoncus erat est sit amet diam. Aliquam dignissim ipsum nec lorem pulvinar.</p>
                                </div>
                                <div class="card-action">
                                    <a class="btn btn-md primary border waves-effect waves-dark" href="{{ url('/jobs/show') }}">Details</a>
                                    <a class="btn btn-md primary waves-effect waves-light" href="#!">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-item" data-groups='["all", "commercial"]'>
                        <div class="portfolio-wrapper">
                            <div class="card job-card">
                                <div class="card-content">
                                    <span class="title">
                                        <span class="card-title">2nd assistant position</span>
                                        <span class="ref grey-text">Ref. 1062</span>
                                    </span>
                                    <div class="metas mb-20">
                                        <div class="meta">
                                            <i class="material-icons">&#xE53E;</i>45k&euro;
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE916;</i>2018-10-01
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE55F;</i>Paris
                                        </div>
                                    </div>
                                    <p class="truncate-text">Nulla interdum, erat et mollis eleifend, urna turpis pellentesque dolor, sed rhoncus erat est sit amet diam. Aliquam dignissim ipsum nec lorem pulvinar.</p>
                                </div>
                                <div class="card-action">
                                    <a class="btn btn-md primary border waves-effect waves-dark" href="{{ url('/jobs/show') }}">Details</a>
                                    <a class="btn btn-md primary waves-effect waves-light" href="#!">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-item" data-groups='["all", "commercial"]'>
                        <div class="portfolio-wrapper">
                            <div class="card job-card">
                                <div class="card-content">
                                    <span class="title">
                                        <span class="card-title">2nd assistant position</span>
                                        <span class="ref grey-text">Ref. 1062</span>
                                    </span>
                                    <div class="metas mb-20">
                                        <div class="meta">
                                            <i class="material-icons">&#xE53E;</i>45k&euro;
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE916;</i>2018-10-01
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE55F;</i>Paris
                                        </div>
                                    </div>
                                    <p class="truncate-text">Nulla interdum, erat et mollis eleifend, urna turpis pellentesque dolor, sed rhoncus erat est sit amet diam. Aliquam dignissim ipsum nec lorem pulvinar.</p>
                                </div>
                                <div class="card-action">
                                    <a class="btn btn-md primary border waves-effect waves-dark" href="{{ url('/jobs/show') }}">Details</a>
                                    <a class="btn btn-md primary waves-effect waves-light" href="#!">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-item" data-groups='["all", "commercial"]'>
                        <div class="portfolio-wrapper">
                            <div class="card job-card">
                                <div class="card-content">
                                    <span class="title">
                                        <span class="card-title">2nd assistant position</span>
                                        <span class="ref grey-text">Ref. 1062</span>
                                    </span>
                                    <div class="metas mb-20">
                                        <div class="meta">
                                            <i class="material-icons">&#xE53E;</i>45k&euro;
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE916;</i>2018-10-01
                                        </div>
                                        <div class="meta">
                                            <i class="material-icons">&#xE55F;</i>Paris
                                        </div>
                                    </div>
                                    <p class="truncate-text">Nulla interdum, erat et mollis eleifend, urna turpis pellentesque dolor, sed rhoncus erat est sit amet diam. Aliquam dignissim ipsum nec lorem pulvinar.</p>
                                </div>
                                <div class="card-action">
                                    <a class="btn btn-md primary border waves-effect waves-dark" href="{{ url('/jobs/show') }}">Details</a>
                                    <a class="btn btn-md primary waves-effect waves-light" href="#!">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="no-item"></div>
                </div>
                <div class="text-center">
                    <a href="{{ url('/jobs') }}" class="btn gradient secondary waves-effect waves-light mt-30">View all job offers</a>
                </div>
            </div>
        </div>
    </section>
@endsection