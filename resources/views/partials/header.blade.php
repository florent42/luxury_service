<header id="header" class="tt-nav transparent-header ">
    <div class="header-sticky semi-transparent">
        <div class="container">

            <div id="materialize-menu" class="menuzord">

                <a class="brand logo-brand" href="{{ url('/')}}"></a>
                <div id="mobile-menu">
                    <div class="nav-wrapper" id="toggle">
                        <span class="top"></span>
                        <span class="middle"></span>
                        <span class="bottom"></span>
                    </div>
                    <div class="icon-wrapper">
                        <span class="account">
							<a href="{{ url('/profil')}}">
                                <i class="material-icons">&#xE7FD;</i>
                            </a>
						</span>
                    </div>
                    <div class="nav-overlay" id="overlay">
                        <nav class="overlay-menu">
                            <ul>
                                <li><a href="{{ url('/')}}">Home</a></li>
                                <li><a href="{{ url('/jobs')}}">Jobs Offers</a></li>
                                <li><a href="{{ url('/about-us')}}">About Us</a></li>
                                <li><a href="{{ url('/contact')}}">Contact</a></li>
                                <li>-</li>
                                <li><a href="{{ url('/profil')}}">Profile</a></li>
                                <li><a href="{{ url('/logout')}}">Logout</a></li>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="menuzord-menu pull-right light" role="navigation">

                        @if (Auth::guest())
                        <li class=" dropdown">
                            <a target="_self" href="{{ url('/login')}}">
                                <span>Login</span>
                            </a>
                        </li>
                        @else
                         <li class=" dropdown">
                            <a target="_self" href="{{ url('/logout')}}">
                                <i class=""></i>
                                <span>Log out</span>
                            </a>
                        </li>
                        @endif
                        <li class=" dropdown">
                            <a target="_self" href="{{ url('/register')}}" class="btn bn-lg gradient secondary btn-block waves-effect waves-light btn-register">
                                <span>Sign Up</span>
                            </a>

                        </li>
                    </ul>
                    <ul class="menuzord-menu pull-right light" role="navigation">
                        <li class="active dropdown">
                            <a target="_self" href="{{ url('/')}}">
                                <i class=""></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ url('/jobs')}}">
                                <i class=""></i>
                                <span>Jobs offers</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ url('/about-us')}}">
                                <i class=""></i>
                                <span>About us</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ url('/contact')}}">
                                <i class=""></i>
                                <span>Contact</span>
                            </a>
                        </li>

                        @if (Auth::check())
                        <li class=" dropdown">
                            <a target="_self" href="{{ url('/profil')}}">
                                <span>Profile</span>
                            </a>
                        </li>
                         @endif
               
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>