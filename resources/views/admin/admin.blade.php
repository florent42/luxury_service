@extends('layouts.layout')

@section('content')

    <section class="gray-bg">
        <div class="container">
            <div class="row">
                <div class="score-container">
                    <br><br>
                    <h3>
                        Admin Space 
                    </h3>
                </div>
            </div>
        </div>
        <br><hr>
    </section>

     <section> 
        <div class="row">
            <div class="col s12">
                  <a href="{{route('listApply')}}">Liste des candidatures </a> 
                  <a href="{{route('listClient')}}"> Liste des clients</a>

                <ul class="tabs">
                    <li class="tab col s6"><a class="active"  href="#createJobs">Création d'une offre d'emploi</a></li>
                    <li class="tab col s6"><a href="#createClient">Création d'un client</a></li>
                    
                 </ul>
            </div>
            <div id="createJobs" class="col s12">
                {{-- Formaulaire création d'offre via l'admin --}}
                @include('admin/partialsAdmin/createOffer')
            </div>

            <div id="createClient" class="col s12">
                {{-- formulaire creation client via l'admin --}}
                @include('admin/partialsAdmin/createClient')
            </div>

         </div>
    </section>
 @endsection