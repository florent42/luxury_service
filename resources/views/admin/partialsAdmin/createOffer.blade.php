
    <form method="POST" action="{{ route('createOffer') }}" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data">

    <input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
            @csrf
        <section class="section-padding">
            <div class="container">
                <div class="row">
                    <h3 class="text-extrabold">Création d'une offre d'emploi</h3>
                    <div class="clearfix visible-sm"></div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <label for="Society_id">Society Name</label>
                            <br>
                            <select id="Society_id" type="select" class="form-control" name="Society_id" value="{{ old('Society_id') }}"  autofocus>

                            @foreach ($clientList as $client )
                                <option selected value="{{$client['id']}}">{{$client['name']}}</option>
                            @endforeach
                                
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="text" class="form-control" name="job_duration" id="job_duration" value="" >
                            <label for="job_duration">Job duration</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="text" class="form-control" name="job_title" id="job_title" value="" >
                            <label for="job_title">Job Title</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="number" class="form-control" name="salary" id="salary" value="" required >
                            <label for="salary">Salary</label>
                        </div>
                    </div>
                
                    <div class="col-xs-12 col-sm-12">
                        <div class="input-field">
                            <textarea class="materialize-textarea" id="description" name="description" cols="50" rows="10"></textarea>
                            <label for="description">Short description for your offers </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="input-field">
                            <label for="jobSector">job Sector</label>
                            <br>
                            <select id="JobSector" type="select" class="form-control{{ $errors->has('JobSector') ? ' is-invalid' : '' }}" name="JobSector" value="{{ old('JobSector') }}"  autofocus>
                                <option selected value="Commercial">Commercial</option>
                                <option value="RetailSales">Retail sales</option>
                                <option value="Creative">Creative</option>
                                <option value="Technology">Technology</option>
                                <option value="Marketing & PR">Marketing & PR</option>
                                <option value="Fashion & Luxury">Fashion & luxury</option>
                                <option value="Management & HR">Management & HR</option>
                            </select>
                            <span class="help-block">jobSector</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="input-field">
                            <label for="jobType">job Type</label>
                            <br>
                            <select id="jobType" type="select" class="form-control{{ $errors->has('jobType') ? ' is-invalid' : '' }}" name="jobType" value="{{ old('jobType') }}"  autofocus>
                                <option selected value="Fulltime">Fulltime</option>
                                <option value="Parttime">Parttime</option>
                                <option value="Temporary">Temporary</option>
                                <option value="Freelance">Freelance</option>
                                <option value="Seasonal">Seasonal</option>
                            </select>
                            <span class="help-block">jobType</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="input-field">
                            <input id="location" name="location" type="text" value="">
                            <label for="location">location</label>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
            <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                <span><strong>CREATE</strong></span>
            </button>
        </div>
    </form>

