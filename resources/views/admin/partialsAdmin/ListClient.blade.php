{{-- liste des clients --}}


@extends('layouts.layout')

@section('content')
        <!-- Page Header-->
        <section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="white-text">Our job offers</h2>
                        <span class="white-text">Search and filter by job category</span>
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li class="active">job offers</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
<!-- Page Content-->
<div class="portfolio-wrapper">
    @foreach($clientList as $client)
        <?php $offerId = $offerList->where('client_id', $client->id) ?> 
                <div class="card job-card">
                    <div class="card-content">
                        <span class="title">
                            <span class="card-title"> {{$client->name}} </span>
                            <span class="ref grey-text"> Ref client: {{$client->id}}</span>
                        </span>
                               
                                    <div class="meta">
                                        <i class="material-icons">date_range</i> {{$client['created_at']->diffForHumans()}}
                                    </div>
                                    <div class="meta">
                                        <i class="material-icons">account_circle</i> {{$client['contact_name']}}
                                    </div>
                                    <div class="meta">
                                        <i class="material-icons">contacts</i> {{$client['contact_post']}}
                                    </div>
                                    <div class="meta">
                                        <i class="material-icons">call</i> {{$client['contact_number']}}
                                    </div>
                                    <div class="meta">
                                        <i class="material-icons">contact_mail</i> {{$client['contact_email']}}
                                </div>
                        <div>offres du client :  
                            @foreach ($offerId as $offer)
                            
                                <a href="{{ url('/jobs/show', $offer->id) }}">{{$offer->title}}</a>
                        
                            @endforeach
                        </div>
                        <div class="card-action">
                            <div class="col-md-4 col-md-offset-3">
                                <form action="{{route('clientDelete')}}">
                                    <button type="submit" onclick="return confirm('Are you sure?')" name="deleteId" class="btn btn-lg gradient red accent-4 waves-effect waves-light" value="{{$client->id}}">Delete Client</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
 </div>
  @endsection

