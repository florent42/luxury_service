{{-- liste des candidatures --}}


@extends('layouts.layout')
@section('content')

<!-- Page Header-->
<section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="white-text">Our job offers</h2>
                <span class="white-text">Search and filter by job category</span>
               
            </div>
        </div>
    </div>
</section>

<div class="portfolio portfolio-with-title col-2 gutter mt-30">
    @foreach($offerList as $offer)
    <?php $applyId = $applyList->where('offer_id', $offer->id) ?> 
        <div class="card job-card">
            <div class="card-content">
                <span class="title">
                    <span class="card-title"> {{$offer->title}} </span>
                    <span class="ref grey-text"> Ref : {{$offer->id}}</span>
                </span>
                <div class="metas mb-20">
                    <div class="meta">
                        <i class="material-icons">&#xE53E;</i> {{$offer['salary']}}k&euro;
                    </div>
                    <div class="meta">
                        <i class="material-icons">&#xE916;</i> {{$offer['created_at']->diffForHumans()}}
                    </div>
                    <div class="meta">
                        <i class="material-icons">&#xE55F;</i> {{$offer['location']}}
                    </div>
                    <div class="meta">
                        <i class="material-icons">business_center</i> {{$offer['category']}}
                    </div>
                </div>
                <p class="truncate-text"> {{$offer['description']}}</p>
                <div>candidats : 
                    @foreach ($applyId as $apply)
                    <?php $userName = $userList->where('id', $apply->user_id) ?> 
                        @foreach($userName as $user)
                            <a href="{{ url('/profil/show', $user->id) }}">{{$user->first_name}} {{$user->last_name}}</a>
                        @endforeach
                    @endforeach
                </div>
                <div class="card-action">
                    <div class="col-md-4 col-md-offset-3">
                        <form action="{{route('offerDelete')}}">
                            <button type="submit" onclick="return confirm('Are you sure?')" name="deleteId" class="btn btn-lg gradient red accent-4 waves-effect waves-light" value="{{$offer->id}}">Delete Offer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection

