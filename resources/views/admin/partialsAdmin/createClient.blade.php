
    <form method="POST" action="{{ route('createClient') }}" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data">
    <input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
        @csrf
        <section class="section-padding">
            <div class="container">
                <div class="row">
                    <h3 class="text-extrabold">Création d'un client</h3>
                    <div class="clearfix visible-sm"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="text" class="form-control" name="Society_Name" id="Society_Name" value="" required>
                            <label for="Society_name">Society Name</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="text" class="form-control" name="contact_name" id="employer_name" value="" >
                            <label for="Employer_name">Contact name</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="text" class="form-control" name="type" id="job_title" value="" >
                            <label for="type">Client type</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input type="text" class="form-control" name="contact_post" id="contact_post" value="" >
                            <label for="contact_post">Contact post</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <input  id="contact_email" name="contact_email" type="email" value="" required>
                            <label for="contact_email">Contact Email</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="input-field">
                            <label for="contact_number">Contact Phone</label>
                            <br>
                            <input  id="contact_number" name="contact_number" type="number" value=""required>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
            <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                <span><strong>CREATE</strong></span>
            </button>
        </div>
    </form>

