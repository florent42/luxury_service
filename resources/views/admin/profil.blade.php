
{{-- profil d'un candidat ayant postulé --}}
@extends('layouts.layout')
@section('content')
   
    <!-- Page Content-->
    <section class="single-project-section section-padding light-gray-bg">
        <div class="container">
            <div class="project-overview">
                <div class="row mb-80">
                   
                    <div class="col-xs-12 col-md-6 quick-overview">
                        <ul class="portfolio-meta">
                            <li><span> First name </span> {{$profilId['first_name']}}</li>
                            <li><span> Last name </span> {{$profilId['last_name']}}</li>
                            <li><span> Country </span> {{$profilId['country']}}</li>
                            <li><span> Nationality </span> {{$profilId['nationality']}}</li>
                            <li><span> Email </span> {{$profilId['email']}}</li>
                            <li><span> Curriculum vitae </span> {{$profilId['cv']}} </li>
                            <li><span> Passport</span> {{$profilId['passport']}}</li>
                            <li><span> Location </span> {{$profilId['location']}}</li>
                            <li><span> Expérience</span> {{$profilId['location']}}</li>
                            <li><span> Job sector </span> {{$profilId['job_sector']}}</li>
                            <li><span> Location </span> {{$profilId['location']}}</li>
                            <li><span> Birthday </span> {{$profilId['birthday']}}</li>
                            <li><span> Availability </span> {{$profilId['user_availability']}}</li>
                           <li><span> Curent location </span> {{$profilId['curent_location']}}</li>
                           <li><span> Picture </span> {{$profilId['picture']}}</li>
                           
                            
                        </ul>

                                                              
                    </div>
                </div>
            </div>

            <nav class="single-post-navigation no-margin" role="navigation">
                <div class="row">

                    <div class="col-md-4 col-md-offset-3">
                        <form action="{{route('profilDelete')}}">
                            <button type="submit" onclick="return confirm('Are you sure?')" name="deleteId" class="btn btn-lg gradient red accent-4 waves-effect waves-light" value="{{$profilId->id}}">Delete Profile</button>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    </section>

    

@endsection('content')