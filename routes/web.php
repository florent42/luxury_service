<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

//routes qui renvoient vers les methodes de nos controller (vue) 

Route::get('/jobs', 'OfferController@index');
Route::get('/about-us', 'HomeController@aboutUs');
Route::get('/contact', 'HomeController@contact');
Route::get('/profil', 'HomeController@profil');
Route::post('/addProfil', 'ProfilController@update')->name('addProfil');
Route::get('/admin', 'HomeController@admin');
Route::post('/admin/offer', 'OfferController@create')->name('createOffer');
Route::post('/admin/client', 'ClientController@create')->name('createClient');
Route::get('/jobs/show/{id}', 'OfferController@show')->name('offerShow');
Route::post('/jobs/apply', 'ApplicationController@apply')->name('apply');
route::get('/offer/delete', 'OfferController@delete')->name('offerDelete');
Route::get('/profil/show/{id}', 'ProfilController@show')->name('profilShow');
route::get('/profil/delete', 'ProfilController@delete')->name('profilDelete');
route::get('/client/delete', 'ClientController@delete')->name('clientDelete');
Route::get('/admin/apply', 'ApplicationController@adminApply')->name('listApply');
Route::get('/admin/client', 'ClientController@adminClient')->name('listClient');
