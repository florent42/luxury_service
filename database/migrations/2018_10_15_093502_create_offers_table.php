<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('reference');
            $table->integer('client_id')->unsigned();
            $table->string('description');
            $table->boolean('active');
            $table->string('note');
            $table->string('title');
            $table->date('offer_availability');
            $table->string('location');
            $table->string('category');
            $table->date('closing_date');
            $table->string('salary');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
