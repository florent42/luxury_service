<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',255);
            $table->string('last_name',255);
            $table->string('email')->unique();
            $table->string('password',255);
            $table->rememberToken();
            $table->timestamps();
            $table->string('country',255);
            $table->string('nationality',255);
            $table->string('note',255);
            $table->boolean('passport_valide');
            $table->string('passport',255);
            $table->string('cv',255);
            $table->string('description',255);
            $table->string('experience',255);
            $table->string('job_sector',255);
            $table->string('user_availability',255);
            $table->integer('birthday');
            $table->string('curent_location',255);
            $table->string('picture',255);
            $table->boolean('is_admin')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
