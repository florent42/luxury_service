<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'offers';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference','description','active','note','title','offer_time_type', 'offer_duration', 'location','category','closing_date','salary','client_id'
    ];

    /**
     * Relation de type 1:n
     * 
     * Get the user that owns the seatingPlan.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
