<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applications';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'offer_id'];
    


    public function offer()
    {
        return $this->hasOne('App\Offer');
    }

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
