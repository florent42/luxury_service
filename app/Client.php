<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','type','contact_name','contact_post','contact_number','contact_number','contact_email','note'
    ];

    /**
     * Relation de type 1:n
     * 
     * Get the user that owns the seatingPlan.
     */
    public function offer()
    {
        return $this->hasMany('App\Offer');
    }

}
