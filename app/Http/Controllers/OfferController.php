<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;

class OfferController extends Controller
{
    protected $offer;

    public function __construct(Offer $offer )
    {
        $this->offer = $offer; 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


//Afiche la liste des offres par catégorie

    public function index()
    {
        $offerList = $this->offer->all();
        $offerCommercial = $this->offer->where('category', 'Commercial')->get();
        $offerRetail = $this->offer->where('category', 'RetailSales')->get();
        $offerCreative = $this->offer->where('category', 'Creative')->get();
        $offerTechnology = $this->offer->where('category', 'Technology')->get();
        $offerMarketing = $this->offer->where('category', 'Marketing & PR')->get();
        $offerFashion = $this->offer->where('category', 'Fashion & Luxury')->get();
        $offerManagement = $this->offer->where('category', 'Management & HR')->get();

        return view('jobs/index', compact('offerList','offerCommercial','offerRetail', 'offerCreative','offerTechnology', 'offerMarketing', 'offerFashion', 'offerManagement'));
    }

//fonction qui envoi les offres crées vers la bdd
    public function create(Request $request)
    {
        $requestData = $request->all();

        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            $this->offer->create(array(
                'client_id' => $requestData['Society_id'],
                'title' => $requestData['job_title'],
                'salary' => $requestData['salary'],
                'description' => $requestData['description'],
                'location' => $requestData['location'],
                'offer_time_type' => $requestData['jobType'],
                'category' => $requestData['JobSector'],  
                'offer_duration' => $requestData['job_duration']
            ));
            return back();
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');

    }
// fonction qui permet d'edit une offre avec son id user qui l'a faite
    public function show($id)
    {
        $offerId = $this->offer->where('id', $id)->first();
        return view('jobs/show', compact('offerId'));
    }

    // fonction qui permet de delete une offre uniquement si on est admin
    public function delete(Request $request)
    {
        $requestData = $request->all();

        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            $this->offer->where('id', $requestData['deleteId'])->delete();
            return back()->with('succes', 'Offer deleted !');
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }
}