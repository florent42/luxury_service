<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use Illuminate\Support\Facades\Auth;
use App\Client;
use App\Offer;
use App\User;

class ApplicationController extends Controller
{
    protected $client;
    protected $user;
    protected $offer;
    protected $application;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client, Offer $offer, Application $application, User $user)
    {
        $this->client = $client;//On transforme le nom des tables de la bdd en variable
        $this->offer = $offer; //            
        $this->user = $user;//                 
        $this->application = $application;//
    }
//Enregistrement d'une candidature
    public function apply(Request $request)
    {
        $requestData = $request->all();// On récupère les inputs du formulaire
        $userId = Auth::id();
        $offerId = $requestData['offer_id'];

        $user = $this->user->find($userId);

// vérifier si le profil est rempli à 100%
        $fields = ['first_name', 'last_name','email', 'country', 'nationality','description','experience', 'birthday'];
        $notEmptyFields = 0;
        foreach($fields as $field) {
            if($user->$field) {
                $notEmptyFields++;
            }
        }
        $completionPercentage = ($notEmptyFields / count($fields)) * 100;
        
 // si le profil est rempli à 100% on éxécute l'enregistrement
        if($completionPercentage == 100){
            if(!$this->application->where(['user_id' => $userId, 'offer_id' => $offerId])->first()){
                $this->application->create(array(
                            'user_id' => $userId,
                            'offer_id' => $offerId,
                        ));//On fait un INSERT INTO dans la table Apllication
                        return back()->with('succes', 'You have applied');
            }
            return back()->with('error', 'you have already apply for this offer');// permet de retourner en arrière avec un message
        }
        return redirect('/profil', compact('completionPercentage'))->with('error', 'Your profil must be at 100 %');
    }

    //Permet d'acceder à la liste des candidatures si on est admin
    public function adminApply()
    {
        $userList = $this->user->all();
        $applyList = $this->application->all();
        $offerList = $this->offer->all();
        $clientList = $this->client->all();


        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            return view('admin/partialsAdmin/ListApply', compact('clientList', 'offerList', 'applyList', 'userList'));
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }
}