<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfilController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        // $this->middelware('auth');
    }

// fonction qui envoie les input du user entrés vers la bdd
    public function update(Request $request)
    {
        $id = Auth::id();
        $requestData = $request->all();
        if($request->file("photo")){
            $photoName = $request->file("photo")->getClientOriginalName();
            $request->file("photo")->storePubliclyAs("photo", $request->file("photo")->getClientOriginalName());
        }
        if($request->file("passport")){
            $passportName = $request->file("passport")->getClientOriginalName();
            $request->file("passport")->storePubliclyAs("passport", $request->file("photo")->getClientOriginalName());
        }
        if($request->file("cv")){
            $cvName = $request->file("cv")->getClientOriginalName();
            $request->file("cv")->storePubliclyAs("cv",$request->file("photo")->getClientOriginalName());
        }
        $this->user->where('id', $id)->update(array(
            'first_name' => $requestData['first_name'],
            'last_name' => $requestData['last_name'],
            'remember_token' => $requestData['_token'],
            'curent_location' => $requestData['current_location'],
            'country' => $requestData['country'],
            'nationality' => $requestData['nationality'],
            'birthday' => $requestData['birth_date'],
            'experience' => $requestData['experience'],
            'description' => $requestData['description'],
            'picture' => 'storage/app/photo/'.$photoName,
            'cv' => 'storage/app/cv/'.$cvName,
            'passport' => 'storage/app/passport/'.$passportName
        ));
        return back()->with('succes', 'Your profile have been updated');
    }

// fonction qui permet d'afficher un profil user uniquement si on est admin
    public function show($id)
    {
        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            $profilId = $this->user->where('id', $id)->first();
            return view('admin/profil', compact('profilId'));
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }


// fonction qui permet de delete un user uniquement si on est admin
    public function delete(Request $request)
    {
        $requestData = $request->all();
        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            $this->user->where('id', $requestData['deleteId'])->delete();
            return back()->with('succes', 'User deleted !');
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }
}