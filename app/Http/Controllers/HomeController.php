<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Offer;
use App\Application;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $client;
    protected $user;
    protected $offer;
    protected $application;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client, Offer $offer, Application $application, User $user)
    {
        // $this->middleware('auth');
        $this->client = $client;
        $this->offer = $offer;
        $this->user = $user;
        $this->application = $application;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutUs()
    {
        return view('company');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('contact');
    }

    public function profil()
    {
        $userId = Auth::id();
        $user = $this->user->find($userId);
        $fields = ['first_name', 'last_name','email', 'country', 'nationality','description','experience', 'birthday'];
        $notEmptyFields = 0;
        foreach($fields as $field) {
            if($user->$field) {
                $notEmptyFields++;
            }
        }
        $completionPercentage = ($notEmptyFields / count($fields)) * 100;
        
        return view('auth/profil', compact('completionPercentage'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        $userList = $this->user->all();
        $applyList = $this->application->all();
        $offerList = $this->offer->all();
        $clientList = $this->client->all(); 

        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            return view('admin/admin', compact('clientList', 'offerList', 'applyList', 'userList'));
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }
}