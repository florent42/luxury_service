<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Offer;
use App\Application;
use App\User;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    protected $client;
    protected $user;
    protected $offer;
    protected $application;

    public function __construct(Client $client, Offer $offer, Application $application, User $user)
    {
        // $this->middleware('auth');
        $this->client = $client;
        $this->offer = $offer;
        $this->user = $user;
        $this->application = $application;
    }

    public function create(Request $request)
    {
        $requestData = $request->all();//On récupère les inputs du formulaire
        
        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            $this->client->create(array(
                'name' => $requestData['Society_Name'],
                'contact_name' => $requestData['contact_name'],
                'type' => $requestData['type'],
                'contact_post' => $requestData['contact_post'],
                'contact_email' => $requestData['contact_email'],
                'contact_number' => $requestData['contact_number']
            ));//On fait un INSERT INTO dans la table client
            return back()->with('succes', 'IT WORKS!');
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }

    public function delete(Request $request)
    {
        $requestData = $request->all();

        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            $this->client->where('id', $requestData['deleteId'])->delete();// On supprime le client via son id
            return back()->with('succes', 'client deleted !');
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }

    public function adminClient()
    {
        $userList = $this->user->all();
        $applyList = $this->application->all();
        $offerList = $this->offer->all();
        $clientList = $this->client->all(); 
        
        if($this->user->where(['id' => Auth::id(), 'is_admin' => 1])->first()){
            return view('admin/partialsAdmin/ListClient', compact('clientList', 'offerList', 'applyList', 'userList'));
        }
            return redirect('/')->with('erroradmin', 'You must be admin to acces this page');
    }
}
